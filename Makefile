.DEFAULT_GOAL := help

GITLAB_CONFIGURATION_FILE=gitlab.application.yml
ADDITIONAL_CONFIGURATION_FILES=$(shell pwd)/$(GITLAB_CONFIGURATION_FILE)

NAME ?= landing-page
CHART ?= chart/$(NAME)
STAGE ?= testing
RELEASE ?= $(NAME)-$(STAGE)
APP_VERSION ?= latest
NAMESPACE ?= cloud-mzimmer-net
DRYRUN ?=

# Show available make targets
.PHONY: help
help:
	@echo "$(notdir $(realpath .)) make targets:"
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: test
test:
	mvn test

.PHONY: run
run:
	export MICRONAUT_CONFIG_FILES=$(ADDITIONAL_CONFIGURATION_FILES); \
		mvn mn:run

.PHONY: deploy-secret
deploy-secret:
	kubectl create secret generic \
		$(NAME) \
		--from-file=gitlab.application.yml=$(GITLAB_CONFIGURATION_FILE) \
		--dry-run=client \
		--output=yaml | kubectl apply --filename=- $(DRYRUN)

.PHONY: deploy
deploy:
	helm upgrade \
		--debug \
		--install \
		--reset-values \
		--values=values.$(STAGE).yml \
		--set image.tag=$(APP_VERSION) \
		--namespace=$(NAMESPACE) \
		--wait \
		$(DRYRUN) \
		$(RELEASE) \
		$(CHART)

.PHONY: dry-run
dry-run:
	@make deploy DRYRUN='--dry-run'

.PHONY: uninstall
uninstall:
	helm --namespace=$(NAMESPACE) uninstall $(RELEASE)
