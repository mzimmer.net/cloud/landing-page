package net.mzimmer.com.gitlab.api;

import io.reactivex.rxjava3.core.Single;
import io.vavr.collection.Seq;

public interface GitLab {
    Api api();

    interface Api {
        V4Api v4();

        interface V4Api {
            GroupsApi groups();

            interface GroupsApi {
                GroupApi id(GroupId groupId);

                interface GroupApi {
                    MilestonesApi milestones();

                    interface MilestonesApi {
                        Single<Seq<Milestone>> get();

                        MilestoneApi id(MilestoneId milestoneId);

                        interface MilestoneApi {
                            IssuesApi issues();

                            interface IssuesApi {
                                Single<Seq<Issue>> get();
                            }
                        }
                    }
                }
            }
        }
    }
}
