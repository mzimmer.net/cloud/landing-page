package net.mzimmer.com.gitlab.api;

public interface Issue {
    boolean isClosed();
}
