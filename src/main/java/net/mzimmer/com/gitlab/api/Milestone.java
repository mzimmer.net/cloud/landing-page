package net.mzimmer.com.gitlab.api;

public interface Milestone {
    MilestoneId getId();

    MilestoneTitle getTitle();
}
