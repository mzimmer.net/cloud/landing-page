package net.mzimmer.com.gitlab.api;

import io.micronaut.core.type.Argument;
import io.micronaut.http.uri.UriBuilder;
import io.reactivex.rxjava3.core.Single;
import io.vavr.collection.Seq;
import io.vavr.collection.Stream;
import lombok.AllArgsConstructor;
import net.mzimmer.com.gitlab.client.GitLabClient;

import javax.inject.Singleton;

import static io.micronaut.http.HttpMethod.GET;

@Singleton
@AllArgsConstructor
public class GitLabImpl implements GitLab {
    private final GitLabClient gitLabClient;

    @Override
    public Api api() {
        return new ApiImpl();
    }

    public class ApiImpl implements GitLab.Api {
        @Override
        public V4Api v4() {
            return new V4ApiImpl();
        }

        public class V4ApiImpl implements GitLab.Api.V4Api {
            @Override
            public GroupsApi groups() {
                return new GroupsApiImpl();
            }

            public class GroupsApiImpl implements GitLab.Api.V4Api.GroupsApi {
                @Override
                public GroupApi id(GroupId groupId) {
                    return new GroupApiImpl(groupId);
                }

                @AllArgsConstructor
                public class GroupApiImpl implements GitLab.Api.V4Api.GroupsApi.GroupApi {
                    private final GroupId groupId;

                    @Override
                    public MilestonesApi milestones() {
                        return new MilestonesApiImpl();
                    }

                    public class MilestonesApiImpl implements GitLab.Api.V4Api.GroupsApi.GroupApi.MilestonesApi {
                        @Override
                        public Single<Seq<Milestone>> get() {
                            return gitLabClient.request(GET,
                                                        UriBuilder.of("/api/v4/groups")
                                                                  .path(String.valueOf(groupId.getValue()))
                                                                  .path("milestones"),
                                                        Argument.listOf(MilestoneResponse.class))
                                               .map(Stream::ofAll)
                                               .map(milestoneResponses -> milestoneResponses.map(MilestoneResponse::extract));
                        }

                        @Override
                        public MilestoneApi id(MilestoneId milestoneId) {
                            return new MilestoneApiImpl(milestoneId);
                        }

                        @AllArgsConstructor
                        public class MilestoneApiImpl implements GitLab.Api.V4Api.GroupsApi.GroupApi.MilestonesApi.MilestoneApi {
                            private final MilestoneId milestoneId;

                            @Override
                            public IssuesApi issues() {
                                return new IssuesApiImpl();
                            }

                            public class IssuesApiImpl implements GitLab.Api.V4Api.GroupsApi.GroupApi.MilestonesApi.MilestoneApi.IssuesApi {
                                @Override
                                public Single<Seq<Issue>> get() {
                                    return gitLabClient.request(GET,
                                                                UriBuilder.of("/api/v4/groups")
                                                                          .path(String.valueOf(groupId.getValue()))
                                                                          .path("milestones")
                                                                          .path(String.valueOf(milestoneId.getValue()))
                                                                          .path("issues"),
                                                                Argument.listOf(IssueResponse.class))
                                                       .map(Stream::ofAll)
                                                       .map(issueResponses -> issueResponses.map(IssueResponse::extract));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
