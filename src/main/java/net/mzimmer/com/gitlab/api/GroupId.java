package net.mzimmer.com.gitlab.api;

import lombok.Value;

@Value
public class GroupId {
    long value;
}
