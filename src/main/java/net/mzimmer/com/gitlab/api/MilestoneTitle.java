package net.mzimmer.com.gitlab.api;

import lombok.NonNull;
import lombok.Value;

@Value
public class MilestoneTitle {
    @NonNull String value;
}
