package net.mzimmer.com.gitlab.api;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

@Data
public class IssueResponse implements Response<Issue> {
    String state;

    @Override
    public Issue extract() {
        return new DefaultIssue(
            "closed".equals(state)
        );
    }

    @Value
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class DefaultIssue implements Issue {
        boolean closed;
    }
}
