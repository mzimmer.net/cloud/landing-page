package net.mzimmer.com.gitlab.api;

public interface Response<T> {
    T extract();
}
