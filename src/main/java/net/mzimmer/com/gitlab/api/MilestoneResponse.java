package net.mzimmer.com.gitlab.api;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

@Data
public class MilestoneResponse implements Response<Milestone> {
    long id;
    String title;

    @Override
    public Milestone extract() {
        return new DefaultMilestone(
            new MilestoneId(id),
            new MilestoneTitle(title)
        );
    }

    @Value
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class DefaultMilestone implements Milestone {
        MilestoneId id;
        MilestoneTitle title;
    }
}
