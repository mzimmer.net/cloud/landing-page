package net.mzimmer.com.gitlab.client;

import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpMethod;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.rxjava3.http.client.Rx3HttpClient;
import io.reactivex.rxjava3.core.Single;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
@AllArgsConstructor
public class GitLabClient {
    private final Rx3HttpClient httpClient;
    private final Optional<GitLabClientConfiguration> gitLabClientConfiguration;

    public <RESPONSE_BODY> Single<RESPONSE_BODY> request(HttpMethod method,
                                                         UriBuilder uriBuilder,
                                                         Argument<RESPONSE_BODY> responseType) {
        return Option.ofOptional(gitLabClientConfiguration).fold(
            () -> Single.error(new GitLabInvalidConfigurationClientException("gitlab.client configuration is missing")),
            configuration -> {
                var baseRequest = HttpRequest.create(method,
                                                     uriBuilder.scheme("https")
                                                               .host(configuration.getHost().getValue())
                                                               .build()
                                                               .toString());
                var authenticatedRequest = configuration.getAccessToken().fold(
                    () -> baseRequest,
                    accessToken -> baseRequest.bearerAuth(accessToken.getValue())
                );
                return Single.fromPublisher(
                    httpClient.retrieve(
                        authenticatedRequest,
                        responseType
                    )
                );
            }
        );
    }
}
