package net.mzimmer.com.gitlab.client;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.vavr.control.Option;
import lombok.*;
import org.apache.commons.lang3.StringUtils;

@Data
@ConfigurationProperties("gitlab.client")
public class GitLabClientConfiguration {
    public static final Host DEFAULT_HOST = new Host("gitlab.com");

    private String host;
    private String accessToken;

    public Host getHost() {
        return Option.of(host).flatMap(Host::of).getOrElse(DEFAULT_HOST);
    }

    public Option<AccessToken> getAccessToken() {
        return Option.of(accessToken).flatMap(AccessToken::of);
    }

    @Value
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Host {
        @NonNull String value;

        public static Option<Host> of(String value) {
            return StringUtils.isNotBlank(value)
                ? Option.of(new Host(value))
                : Option.none();
        }
    }

    @Value
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class AccessToken {
        @NonNull String value;

        public static Option<AccessToken> of(String value) {
            return StringUtils.isNotBlank(value)
                ? Option.of(new AccessToken(value))
                : Option.none();
        }
    }
}
