package net.mzimmer.com.gitlab.client;

public abstract class GitLabClientException extends RuntimeException {
    public GitLabClientException() {
        super();
    }

    public GitLabClientException(String message) {
        super(message);
    }

    public GitLabClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public GitLabClientException(Throwable cause) {
        super(cause);
    }

    protected GitLabClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
