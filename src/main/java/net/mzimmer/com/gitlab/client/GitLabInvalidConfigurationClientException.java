package net.mzimmer.com.gitlab.client;

public class GitLabInvalidConfigurationClientException extends GitLabClientException {
    public GitLabInvalidConfigurationClientException(String message) {
        super(message);
    }
}
