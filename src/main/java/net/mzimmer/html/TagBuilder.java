package net.mzimmer.html;

public interface TagBuilder extends Tag {
    TagBuilder a(String name, String value);

    TagBuilder attribute(String name, String value);

    Tag c(Node... tags);

    Tag children(Node... tags);

    Tag c(Iterable<Node> tags);

    Tag children(Iterable<Node> tags);

    Tag c(String text);

    Tag children(String text);
}
