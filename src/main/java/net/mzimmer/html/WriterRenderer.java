package net.mzimmer.html;

import io.vavr.CheckedConsumer;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.io.Writer;

@AllArgsConstructor
public class WriterRenderer implements Renderer<Void> {
    private final Writer writer;

    @SneakyThrows
    @Override
    public Void render(TextNode textNode) {
        writer.append(textNode.getText());
        return null;
    }

    @SneakyThrows
    @Override
    public Void render(Tag tag) {
        writer.append('<');
        writer.append(tag.getName().getValue());
        tag.getAttributes().forEach(((CheckedConsumer<Attribute>) attribute -> {
            writer.append(' ');
            writer.append(attribute.getName().getValue());
            writer.append("=\"");
            writer.append(attribute.getValue().getValue());
            writer.append('"');
        }).unchecked());
        writer.append('>');
        if (tag.getChildren().isEmpty() && shouldNotBeClosed(tag)) {
            return null;
        }
        tag.getChildren().forEach(this::render);
        writer.append("</");
        writer.append(tag.getName().getValue());
        writer.append('>');
        return null;
    }

    @Override
    public Void join(Void a, Void b) {
        return null;
    }
}
