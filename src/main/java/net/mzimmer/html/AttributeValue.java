package net.mzimmer.html;

import lombok.Value;

@Value
public class AttributeValue {
    String value;
}
