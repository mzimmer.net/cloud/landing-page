package net.mzimmer.html;

public class StringRenderer implements Renderer<String> {
    @Override
    public String render(TextNode textNode) {
        return textNode.getText();
    }

    @Override
    public String render(Tag tag) {
        final String openingTag = "<" + tag.getName().getValue() + tag.getAttributes().map(this::render).mkString() + ">";

        if (tag.getChildren().isEmpty() && shouldNotBeClosed(tag)) {
            return openingTag;
        }

        final String children = tag.getChildren().map(this::render).mkString();
        final String closingTag = "</" + tag.getName().getValue() + ">";
        return openingTag + children + closingTag;
    }

    private String render(Attribute attribute) {
        return " " + attribute.getName().getValue() + "=\"" + attribute.getValue().getValue() + "\"";
    }

    @Override
    public String join(String a, String b) {
        return a + b;
    }
}
