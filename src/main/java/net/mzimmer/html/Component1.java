package net.mzimmer.html;

import io.vavr.Function1;
import io.vavr.collection.Stream;

@FunctionalInterface
public interface Component1<P1> extends Function1<P1, Stream<Node>> {
    Stream<Node> build(P1 p1);

    @Override
    default Stream<Node> apply(P1 p1) {
        return build(p1);
    }
}
