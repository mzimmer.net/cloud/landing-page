package net.mzimmer.html;

import lombok.Value;

@Value
public class AttributeName {
    String value;
}
