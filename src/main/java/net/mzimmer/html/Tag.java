package net.mzimmer.html;

import io.vavr.collection.Stream;

public interface Tag extends ParentNode {
    TagName getName();

    Stream<Attribute> getAttributes();
}
