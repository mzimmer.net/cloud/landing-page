package net.mzimmer.html;

public interface TextNode extends Node {
    String getText();
}
