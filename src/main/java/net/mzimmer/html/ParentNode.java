package net.mzimmer.html;

import io.vavr.collection.Stream;

public interface ParentNode extends Node {
    Stream<Node> getChildren();
}
