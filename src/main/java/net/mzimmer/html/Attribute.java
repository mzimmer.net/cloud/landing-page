package net.mzimmer.html;

import lombok.Value;

@Value
public class Attribute {
    AttributeName name;

    AttributeValue value;
}
