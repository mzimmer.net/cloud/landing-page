package net.mzimmer.html;

import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.collection.Stream;

public interface Renderer<OUTPUT> {
    default OUTPUT render(Stream<Node> nodes) {
        return nodes.map(this::render).reduce(this::join);
    }

    default OUTPUT render(Node node) {
        return node instanceof TextNode ?
            render((TextNode) node) :
            node instanceof Tag ?
                render((Tag) node) :
                renderUnknownNodeType(node);
    }

    default OUTPUT renderUnknownNodeType(Node node) {
        throw new IllegalStateException("Unknown node type " + node.getClass().getCanonicalName() + " of node " + node);
    }

    OUTPUT render(TextNode textNode);

    OUTPUT render(Tag tag);

    Array<TagName> VOID_ELEMENT_TAG_NAMES = Array.of(
        "area",
        "base",
        "br",
        "col",
        "command",
        "embed",
        "hr",
        "img",
        "input",
        "keygen",
        "link",
        "meta",
        "param",
        "source",
        "track",
        "wbr"
    ).map(TagName::new);

    default Seq<TagName> getVoidElementTagNames() {
        return VOID_ELEMENT_TAG_NAMES;
    }

    default boolean shouldNotBeClosed(Tag tag) {
        return getVoidElementTagNames().contains(tag.getName());
    }

    OUTPUT join(OUTPUT a, OUTPUT b);
}
