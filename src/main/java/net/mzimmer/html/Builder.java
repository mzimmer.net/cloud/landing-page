package net.mzimmer.html;

import io.vavr.collection.Stream;
import lombok.Value;

public final class Builder {
    private Builder() {
    }

    public static Node doctype(String doctype) {
        return new DefaultTextNode("<!doctype " + doctype + ">");
    }

    public static TagBuilder t(String name) {
        return tag(name);
    }

    public static TagBuilder tag(String name) {
        return new DefaultTagBuilder(new TagName(name), Stream.empty());
    }

    @Value
    private static class DefaultTagBuilder implements TagBuilder {
        TagName name;
        Stream<Attribute> attributes;

        @Override
        public TagBuilder a(String name, String value) {
            return attribute(name, value);
        }

        @Override
        public TagBuilder attribute(String name, String value) {
            return new DefaultTagBuilder(this.name, attributes.append(new Attribute(new AttributeName(name), new AttributeValue(value))));
        }

        @Override
        public Tag c(Node... tags) {
            return children(tags);
        }

        @Override
        public Tag children(Node... tags) {
            return withChildren(Stream.of(tags));
        }

        @Override
        public Tag c(Iterable<Node> tags) {
            return children(tags);
        }

        @Override
        public Tag children(Iterable<Node> tags) {
            return withChildren(Stream.ofAll(tags));
        }

        @Override
        public Tag c(String text) {
            return children(text);
        }

        @Override
        public Tag children(String text) {
            return withChildren(Stream.of(new DefaultTextNode(text)));
        }

        @Override
        public Stream<Node> getChildren() {
            return Stream.empty();
        }

        private Tag withChildren(Stream<Node> children) {
            return new DefaultTag(name, attributes, children);
        }
    }

    @Value
    private static class DefaultTextNode implements TextNode {
        String text;
    }

    @Value
    private static class DefaultTag implements Tag {
        TagName name;
        Stream<Attribute> attributes;
        Stream<Node> children;
    }
}

