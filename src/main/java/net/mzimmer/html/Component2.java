package net.mzimmer.html;

import io.vavr.Function2;
import io.vavr.collection.Stream;

@FunctionalInterface
public interface Component2<P1, P2> extends Function2<P1, P2, Stream<Node>> {
    Stream<Node> build(P1 p1, P2 p2);

    @Override
    default Stream<Node> apply(P1 p1, P2 p2) {
        return build(p1, p2);
    }
}
