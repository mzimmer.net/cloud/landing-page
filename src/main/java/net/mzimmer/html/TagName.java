package net.mzimmer.html;

import lombok.Value;

@Value
public class TagName {
    String value;
}
