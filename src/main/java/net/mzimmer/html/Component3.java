package net.mzimmer.html;

import io.vavr.Function3;
import io.vavr.collection.Stream;

@FunctionalInterface
public interface Component3<P1, P2, P3> extends Function3<P1, P2, P3, Stream<Node>> {
    Stream<Node> build(P1 p1, P2 p2, P3 p3);

    @Override
    default Stream<Node> apply(P1 p1, P2 p2, P3 p3) {
        return build(p1, p2, p3);
    }
}
