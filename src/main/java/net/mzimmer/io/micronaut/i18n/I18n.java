package net.mzimmer.io.micronaut.i18n;

import io.micronaut.context.MessageSource;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.text.MessageFormat;

@Slf4j
@Singleton
@AllArgsConstructor
public class I18n {
    private final MessageSource messageSource;

    public String m(MessageSource.MessageContext messageContext, String code, Object... args) {
        return messageSource.getRawMessage(code, messageContext)
                            .map(message -> new MessageFormat(message, messageContext.getLocale()).format(args))
                            .orElseGet(() -> {
                                LOG.error("Message missing for code <" + code + "> and context with locale <" + messageContext.getLocale() + ">");
                                return code;
                            });
    }
}
