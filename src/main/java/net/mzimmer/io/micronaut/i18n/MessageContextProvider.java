package net.mzimmer.io.micronaut.i18n;

import io.micronaut.context.MessageSource;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpParameters;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import net.mzimmer.cloud.landingpage.boot.I18nConfiguration;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Singleton;
import java.util.Locale;

@Singleton
@AllArgsConstructor
public class MessageContextProvider {
    private static final Locale FALLBACK_LOCALE = Locale.GERMAN;

    private final I18nConfiguration i18nConfiguration;

    public MessageSource.MessageContext getMessageContext(HttpParameters httpParameters, HttpHeaders httpHeaders) {
        final Option<String> maybeParameterHint = Option.ofOptional(httpParameters.getFirst("lang"));
        final Option<String> maybeHint = maybeParameterHint.orElse(
            () -> Option.ofOptional(httpHeaders.getFirst(HttpHeaders.ACCEPT_LANGUAGE))
                        .map(acceptLanguageHeader -> StringUtils.substringBefore(acceptLanguageHeader, ","))
        );
        final Option<Locale> maybeHintedLocale = maybeHint.map(Locale::forLanguageTag);
        final Option<Locale> maybeAllowedLocale = maybeHintedLocale.flatMap(
            hintedLocale -> i18nConfiguration.getAllowedLanguages().contains(hintedLocale.getLanguage()) ?
                Option.some(hintedLocale) :
                Option.none()
        );
        final Locale locale = maybeAllowedLocale.getOrElse(FALLBACK_LOCALE);
        return MessageSource.MessageContext.of(locale);
    }
}
