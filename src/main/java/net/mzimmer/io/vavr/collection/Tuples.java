package net.mzimmer.io.vavr.collection;

import io.vavr.Tuple;
import io.vavr.Tuple3;
import io.vavr.Tuple4;
import io.vavr.Tuple5;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Tuples {
    public <T1, T2, T3, T4> Tuple3<T2, T3, T4> tail3(Tuple4<T1, T2, T3, T4> tuple) {
        return Tuple.of(tuple._2(), tuple._3(), tuple._4());
    }

    public <T1, T2, T3, T4, T5> Tuple4<T2, T3, T4, T5> tail4(Tuple5<T1, T2, T3, T4, T5> tuple) {
        return Tuple.of(tuple._2(), tuple._3(), tuple._4(), tuple._5());
    }
}
