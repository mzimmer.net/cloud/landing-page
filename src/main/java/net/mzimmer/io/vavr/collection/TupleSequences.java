package net.mzimmer.io.vavr.collection;

import io.vavr.Tuple3;
import io.vavr.Tuple4;
import io.vavr.Tuple5;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TupleSequences {
    public <T1, T2, T3, T4> Map<T1, Seq<Tuple3<T2, T3, T4>>> group4By1And3(Seq<Tuple4<T1, T2, T3, T4>> seq) {
        return seq.groupBy(Tuple4::_1).mapValues(group -> group.map(Tuples::tail3));
    }

    public <T1, T2, T3, T4, T5> Map<T1, Seq<Tuple4<T2, T3, T4, T5>>> group5By1And4(Seq<Tuple5<T1, T2, T3, T4, T5>> seq) {
        return seq.groupBy(Tuple5::_1).mapValues(group -> group.map(Tuples::tail4));
    }
}
