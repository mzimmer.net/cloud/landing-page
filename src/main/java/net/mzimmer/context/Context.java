package net.mzimmer.context;

import io.reactivex.rxjava3.core.Single;
import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.concurrent.Future;

import java.util.concurrent.TimeUnit;

public interface Context<T> {
    T extract();

    <U> Context<U> map(Function1<T, U> mapper);

    <U> Context<U> flatMap(Function1<T, Context<U>> mapper);

    static <T> Context<T> id(T value) {
        return new IdContext<>(value);
    }

    static <T> Context<T> future(Future<T> future) {
        return new FutureContext<>(future);
    }

    static <T> Context<T> future(Future<T> future, Tuple2<Long, TimeUnit> timeout) {
        return new FutureContext<>(future, timeout);
    }

    Future<T> asFuture();

    static <T> Context<T> single(Single<T> single) {
        return new SingleContext<>(single);
    }

    Single<T> asSingle();
}
