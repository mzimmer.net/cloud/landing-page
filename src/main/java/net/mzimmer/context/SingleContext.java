package net.mzimmer.context;

import io.reactivex.rxjava3.core.Single;
import io.vavr.Function1;
import io.vavr.concurrent.Future;

public class SingleContext<T> implements Context<T> {
    private final Single<T> single;

    SingleContext(Single<T> single) {
        this.single = single;
    }

    @Override
    public T extract() {
        return single.blockingGet();
    }

    @Override
    public <U> Context<U> map(Function1<T, U> mapper) {
        return new SingleContext<>(single.map(mapper::apply));
    }

    @Override
    public <U> Context<U> flatMap(Function1<T, Context<U>> mapper) {
        return new SingleContext<>(single.flatMap(value -> {
            Context<U> result = mapper.apply(value);
            return result instanceof SingleContext
                ? ((SingleContext<U>) result).single
                : Single.just(result.extract());
        }));
    }

    @Override
    public Future<T> asFuture() {
        return Future.fromJavaFuture(single.toFuture());
    }

    @Override
    public Single<T> asSingle() {
        return single;
    }
}
