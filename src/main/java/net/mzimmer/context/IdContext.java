package net.mzimmer.context;

import io.reactivex.rxjava3.core.Single;
import io.vavr.Function1;
import io.vavr.concurrent.Future;

public final class IdContext<T> implements Context<T> {
    private final T value;

    IdContext(T value) {
        this.value = value;
    }

    @Override
    public T extract() {
        return value;
    }

    @Override
    public <U> Context<U> map(Function1<T, U> mapper) {
        return new IdContext<>(mapper.apply(value));
    }

    @Override
    public <U> Context<U> flatMap(Function1<T, Context<U>> mapper) {
        return mapper.apply(value);
    }

    @Override
    public Future<T> asFuture() {
        return Future.successful(value);
    }

    @Override
    public Single<T> asSingle() {
        return Single.just(value);
    }
}
