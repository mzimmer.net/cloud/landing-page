package net.mzimmer.context;

import io.reactivex.rxjava3.core.Single;
import io.vavr.Function1;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.concurrent.Future;

import java.util.concurrent.TimeUnit;

public final class FutureContext<T> implements Context<T> {
    public static final Tuple2<Long, TimeUnit> DEFAULT_TIMEOUT = Tuple.of(3L, TimeUnit.SECONDS);

    private final Future<T> future;
    private final Tuple2<Long, TimeUnit> timeout;

    FutureContext(Future<T> future) {
        this(future, DEFAULT_TIMEOUT);
    }

    FutureContext(Future<T> future, Tuple2<Long, TimeUnit> timeout) {
        this.future = future;
        this.timeout = timeout;
    }

    @Override
    public T extract() {
        return future.await(timeout._1(), timeout._2()).getOrNull();
    }

    @Override
    public <U> Context<U> map(Function1<T, U> mapper) {
        return new FutureContext<>(future.map(mapper), timeout);
    }

    @Override
    public <U> Context<U> flatMap(Function1<T, Context<U>> mapper) {
        return new FutureContext<>(future.flatMap(value -> {
            Context<U> result = mapper.apply(value);
            return result instanceof net.mzimmer.context.FutureContext
                ? ((FutureContext<U>) result).future
                : Future.of(result::extract);
        }), timeout);
    }

    @Override
    public Future<T> asFuture() {
        return future;
    }

    @Override
    public Single<T> asSingle() {
        return Tuple.of(future.toCompletableFuture()).concat(timeout).apply(Single::fromFuture);
    }
}
