package net.mzimmer.cloud.landingpage.boot;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.vavr.Tuple;
import io.vavr.collection.Array;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import lombok.Data;
import net.mzimmer.cloud.landingpage.domain.PhaseId;
import net.mzimmer.cloud.landingpage.domain.ProjectId;
import net.mzimmer.com.gitlab.api.GroupId;
import net.mzimmer.com.gitlab.api.MilestoneId;

import java.util.Locale;

@Data
@ConfigurationProperties("landingpage.gitlab")
public class LandingPageGitLabConfiguration {
    private java.util.Map<Long, String> groupProjectMapping;
    private java.util.Map<Long, String> milestonePhaseMapping;

    public Map<GroupId, ProjectId> getGroupProjectMapping() {
        return HashMap.ofAll(groupProjectMapping)
                      .flatMap((rawGroupId, rawProjectId) ->
                                   Array.of(ProjectId.values())
                                        .find(projectId -> projectId.toString().equals(rawProjectId.toUpperCase(Locale.ROOT)))
                                        .map(Tuple.of(new GroupId(rawGroupId))::append)
                      );
    }

    public Map<MilestoneId, PhaseId> getMilestonePhaseMapping() {
        return HashMap.ofAll(milestonePhaseMapping)
                      .flatMap((rawMilestoneId, rawPhaseId) ->
                                   Array.of(PhaseId.values())
                                        .find(phaseId -> phaseId.toString().equals(rawPhaseId.toUpperCase(Locale.ROOT)))
                                        .map(Tuple.of(new MilestoneId(rawMilestoneId))::append)
                      );
    }
}
