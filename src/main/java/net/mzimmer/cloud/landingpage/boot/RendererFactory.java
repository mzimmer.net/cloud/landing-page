package net.mzimmer.cloud.landingpage.boot;

import io.micronaut.context.annotation.Factory;
import net.mzimmer.html.Renderer;
import net.mzimmer.html.StringRenderer;

import javax.inject.Singleton;

@Factory
public class RendererFactory {
    @Singleton
    public Renderer<String> getStringRenderer() {
        return new StringRenderer();
    }
}
