package net.mzimmer.cloud.landingpage.boot;

import io.micronaut.context.MessageSource;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.i18n.ResourceBundleMessageSource;

import javax.inject.Singleton;

@Factory
public class MessageSourceFactory {
    @Singleton
    public MessageSource getMessageSource() {
        return new ResourceBundleMessageSource("messages");
    }
}
