package net.mzimmer.cloud.landingpage.boot;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import lombok.Data;

@Data
@ConfigurationProperties("landingpage.i18n")
public class I18nConfiguration {
    private Seq<String> allowedLanguages;

    public void setAllowedLanguages(java.util.List<String> allowedLanguages) {
        this.allowedLanguages = List.ofAll(allowedLanguages);
    }
}
