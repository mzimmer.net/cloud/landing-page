package net.mzimmer.cloud.landingpage.domain;

import lombok.Value;

@Value
public class NumberOfIssues {
    public static final NumberOfIssues ZERO = new NumberOfIssues(0);

    int value;

    public NumberOfIssues plus(NumberOfIssues other) {
        return new NumberOfIssues(value + other.value);
    }
}
