package net.mzimmer.cloud.landingpage.domain;

import io.vavr.Tuple0;
import io.vavr.collection.SortedSet;
import net.mzimmer.context.Context;

public interface MilestoneWritableRepository extends MilestoneRepository {
    Context<Tuple0> saveMilestones(SortedSet<Milestone> milestones);
}
