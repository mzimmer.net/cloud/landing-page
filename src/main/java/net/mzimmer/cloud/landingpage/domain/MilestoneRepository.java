package net.mzimmer.cloud.landingpage.domain;

import io.vavr.collection.SortedSet;
import net.mzimmer.context.Context;

public interface MilestoneRepository {
    Context<SortedSet<Milestone>> allMilestones();
}
