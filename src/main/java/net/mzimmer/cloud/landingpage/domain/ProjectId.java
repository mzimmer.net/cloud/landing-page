package net.mzimmer.cloud.landingpage.domain;

public enum ProjectId {
    INFRASTRUCTURE,
    CLOUD
}
