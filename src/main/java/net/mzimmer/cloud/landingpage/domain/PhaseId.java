package net.mzimmer.cloud.landingpage.domain;

public enum PhaseId {
    PRE_ALPHA,
    ALPHA,
    BETA,
    RELEASE
}
