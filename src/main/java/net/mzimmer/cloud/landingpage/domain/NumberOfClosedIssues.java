package net.mzimmer.cloud.landingpage.domain;

import lombok.Value;

@Value
public class NumberOfClosedIssues {
    public static final NumberOfClosedIssues ZERO = new NumberOfClosedIssues(0);

    int value;

    public NumberOfClosedIssues plus(NumberOfClosedIssues other) {
        return new NumberOfClosedIssues(value + other.value);
    }
}
