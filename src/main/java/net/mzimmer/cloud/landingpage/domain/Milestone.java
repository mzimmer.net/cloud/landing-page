package net.mzimmer.cloud.landingpage.domain;

import io.vavr.Lazy;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;

import java.util.Comparator;

@Value
public class Milestone implements Comparable<Milestone> {
    public static final Comparator<Milestone> COMPARATOR = Comparator.comparing(Milestone::getPhaseId)
                                                                     .thenComparing(Milestone::getProjectId);

    @NonNull Name name;
    @NonNull NumberOfIssues numberOfIssues;
    @NonNull NumberOfClosedIssues numberOfClosedIssues;
    @NonNull ProjectId projectId;
    @NonNull PhaseId phaseId;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    Lazy<Progress> progress = Lazy.of(this::calculateProgress);

    public Progress getProgress() {
        return progress.get();
    }

    private Progress calculateProgress() {
        return new Progress(numberOfClosedIssues, numberOfIssues);
    }

    @Override
    public int compareTo(Milestone milestone) {
        return COMPARATOR.compare(this, milestone);
    }

    @Value
    public static class Name {
        @NonNull String value;
    }
}
