package net.mzimmer.cloud.landingpage.domain;

import io.vavr.collection.SortedMap;
import io.vavr.collection.Traversable;
import io.vavr.collection.TreeMap;
import io.vavr.collection.TreeSet;
import io.vavr.control.Option;

import javax.inject.Singleton;

import static java.util.Comparator.naturalOrder;

@Singleton
public class MilestoneCalculationService {
    public Option<OverallProgress> calculateOverallProgress(Traversable<Milestone> milestones) {
        return milestones.headOption().map(firstMilestone -> {
            final Progress overallProgress = calculateProgress(milestones.toStream());
            final SortedMap<PhaseId, OverallProgress.Phase> progressPerPhase = calculateProgressPerPhase(milestones.toStream());
            return new OverallProgress(overallProgress, progressPerPhase);
        });
    }

    public SortedMap<PhaseId, OverallProgress.Phase> calculateProgressPerPhase(Traversable<Milestone> milestones) {
        final var groupedMilestones = milestones.groupBy(Milestone::getPhaseId);
        final var groupedProgress = groupedMilestones.mapValues(this::asPhase);
        return TreeMap.ofEntries(naturalOrder(), groupedProgress);
    }

    public OverallProgress.Phase asPhase(Traversable<Milestone> milestonePerPhase) {
        final Progress progress = calculateProgress(milestonePerPhase);
        final TreeSet<Milestone> sortedMilestonesPerPhase = TreeSet.ofAll(milestonePerPhase);
        return new OverallProgress.Phase(progress, sortedMilestonesPerPhase);
    }

    public Progress calculateProgress(Traversable<Milestone> milestones) {
        return new Progress(calculateNumberOfClosedIssues(milestones),
                            calculateNumberOfIssues(milestones));
    }

    public NumberOfClosedIssues calculateNumberOfClosedIssues(Traversable<Milestone> milestones) {
        return milestones.map(Milestone::getNumberOfClosedIssues).fold(NumberOfClosedIssues.ZERO, NumberOfClosedIssues::plus);
    }

    public NumberOfIssues calculateNumberOfIssues(Traversable<Milestone> milestones) {
        return milestones.map(Milestone::getNumberOfIssues).fold(NumberOfIssues.ZERO, NumberOfIssues::plus);
    }
}
