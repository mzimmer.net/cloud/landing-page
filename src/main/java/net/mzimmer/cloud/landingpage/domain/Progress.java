package net.mzimmer.cloud.landingpage.domain;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Value
public class Progress {
    NumberOfClosedIssues numberOfClosedIssues;
    NumberOfIssues numberOfIssues;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    int percentage;

    public Progress(NumberOfClosedIssues numberOfClosedIssues, NumberOfIssues numberOfIssues) {
        this.numberOfClosedIssues = numberOfClosedIssues;
        this.numberOfIssues = numberOfIssues;
        this.percentage = calculatePercentage();
    }

    private int calculatePercentage() {
        return numberOfIssues.getValue() != 0
            ? BigDecimal.valueOf(numberOfClosedIssues.getValue())
                        .multiply(BigDecimal.valueOf(100L))
                        .divide(BigDecimal.valueOf(numberOfIssues.getValue()), RoundingMode.DOWN)
                        .intValue()
            : 0;
    }
}
