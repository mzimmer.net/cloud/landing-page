package net.mzimmer.cloud.landingpage.domain;

import io.vavr.collection.SortedMap;
import io.vavr.collection.SortedSet;
import lombok.Value;

@Value
public class OverallProgress {
    Progress overallProgress;

    SortedMap<PhaseId, Phase> phases;

    @Value
    public static class Phase {
        Progress progress;
        SortedSet<Milestone> milestones;
    }
}
