package net.mzimmer.cloud.landingpage.application.model;

import io.vavr.control.Option;
import lombok.Value;
import net.mzimmer.cloud.landingpage.domain.OverallProgress;

@Value
public class GetOverallProgressCommandResult {
    Option<OverallProgress> maybeOverallProgress;
}
