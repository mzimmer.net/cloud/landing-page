package net.mzimmer.cloud.landingpage.application;

import io.vavr.Tuple0;
import lombok.AllArgsConstructor;
import net.mzimmer.cloud.landingpage.application.model.GetOverallProgressCommandResult;
import net.mzimmer.cloud.landingpage.domain.MilestoneCachedRepository;
import net.mzimmer.cloud.landingpage.domain.MilestoneCalculationService;
import net.mzimmer.cloud.landingpage.domain.MilestoneUpstreamRepository;
import net.mzimmer.context.Context;

import javax.inject.Singleton;

@Singleton
@AllArgsConstructor
public class ProgressApplicationService {
    private final MilestoneCachedRepository milestoneCachedRepository;
    private final MilestoneUpstreamRepository milestoneUpstreamRepository;
    private final MilestoneCalculationService milestoneCalculationService;

    public Context<Tuple0> refreshMilestones() {
        return milestoneUpstreamRepository.allMilestones().flatMap(milestoneCachedRepository::saveMilestones);
    }

    public Context<GetOverallProgressCommandResult> getOverallProgress() {
        return milestoneCachedRepository.allMilestones()
                                        .map(milestoneCalculationService::calculateOverallProgress)
                                        .map(GetOverallProgressCommandResult::new);
    }
}
