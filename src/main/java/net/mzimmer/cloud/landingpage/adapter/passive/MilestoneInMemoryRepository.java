package net.mzimmer.cloud.landingpage.adapter.passive;

import io.vavr.Tuple;
import io.vavr.Tuple0;
import io.vavr.collection.SortedSet;
import io.vavr.collection.TreeSet;
import net.mzimmer.cloud.landingpage.domain.Milestone;
import net.mzimmer.cloud.landingpage.domain.MilestoneCachedRepository;
import net.mzimmer.context.Context;

import javax.inject.Singleton;
import java.util.concurrent.atomic.AtomicReference;

@Singleton
public class MilestoneInMemoryRepository implements MilestoneCachedRepository {
    private final AtomicReference<SortedSet<Milestone>> milestones = new AtomicReference<>(TreeSet.empty());

    @Override
    public Context<Tuple0> saveMilestones(SortedSet<Milestone> milestones) {
        this.milestones.set(milestones);
        return Context.id(Tuple.empty());
    }

    @Override
    public Context<SortedSet<Milestone>> allMilestones() {
        return Context.id(milestones.get());
    }
}
