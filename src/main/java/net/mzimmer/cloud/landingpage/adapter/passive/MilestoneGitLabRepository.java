package net.mzimmer.cloud.landingpage.adapter.passive;

import io.reactivex.rxjava3.core.Single;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.Seq;
import io.vavr.collection.SortedSet;
import io.vavr.collection.Stream;
import io.vavr.collection.TreeSet;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import net.mzimmer.cloud.landingpage.boot.LandingPageGitLabConfiguration;
import net.mzimmer.cloud.landingpage.domain.Milestone;
import net.mzimmer.cloud.landingpage.domain.MilestoneUpstreamRepository;
import net.mzimmer.cloud.landingpage.domain.NumberOfClosedIssues;
import net.mzimmer.cloud.landingpage.domain.NumberOfIssues;
import net.mzimmer.com.gitlab.api.GitLab;
import net.mzimmer.com.gitlab.api.Issue;
import net.mzimmer.context.Context;

import javax.inject.Singleton;

@Singleton
@AllArgsConstructor
public class MilestoneGitLabRepository implements MilestoneUpstreamRepository {
    private final GitLab gitLab;
    private final LandingPageGitLabConfiguration landingPageGitLabConfiguration;

    @Override
    public Context<SortedSet<Milestone>> allMilestones() {
        return Context.single(collapse(Stream.ofAll(landingPageGitLabConfiguration.getGroupProjectMapping().keySet())
                                             .map(groupId ->
                                                      gitLab.api()
                                                            .v4()
                                                            .groups()
                                                            .id(groupId)
                                                            .milestones()
                                                            .get()
                                                            .flatMap(milestones ->
                                                                         collapse(milestones.map(milestone ->
                                                                                                     gitLab.api()
                                                                                                           .v4()
                                                                                                           .groups()
                                                                                                           .id(groupId)
                                                                                                           .milestones()
                                                                                                           .id(milestone.getId())
                                                                                                           .issues()
                                                                                                           .get()
                                                                                                           .map(Tuple.of(milestone)::append)
                                                                         ))
                                                            )
                                                            .map(Tuple.of(groupId)::append)
                                             ))
                                  .map(groups ->
                                           groups.flatMap(group -> {
                                               var groupId = group._1();
                                               var milestones = group._2();

                                               return milestones.flatMap(milestoneWithIssues -> {
                                                   var milestone = milestoneWithIssues._1();
                                                   var issues = milestoneWithIssues._2();

                                                   var name = new Milestone.Name(milestone.getTitle().getValue());
                                                   var numberOfIssues = new NumberOfIssues(issues.length());
                                                   var numberOfClosedIssues = new NumberOfClosedIssues(issues.count(Issue::isClosed));
                                                   var maybeProjectId = landingPageGitLabConfiguration.getGroupProjectMapping().get(groupId);
                                                   var maybePhaseId = landingPageGitLabConfiguration.getMilestonePhaseMapping().get(milestone.getId());

                                                   return collapse(maybeProjectId, maybePhaseId)
                                                       .map(projectIdAndPhaseId -> new Milestone(name,
                                                                                                 numberOfIssues,
                                                                                                 numberOfClosedIssues,
                                                                                                 projectIdAndPhaseId._1(),
                                                                                                 projectIdAndPhaseId._2()));
                                               });
                                           })))
                      .map(TreeSet::ofAll);
    }

    private <T1, T2> Option<Tuple2<T1, T2>> collapse(Option<T1> maybeData1, Option<T2> maybeData2) {
        return maybeData1.flatMap(data1 -> maybeData2.map(Tuple.of(data1)::append));
    }

    private <T> Single<Seq<T>> collapse(Seq<Single<T>> data) {
        return data.foldLeft(Single.just(Stream.empty()), (collected, next) -> Single.zip(collected, next, Seq::append));
    }
}
