package net.mzimmer.cloud.landingpage.adapter.active.ui;

import io.micronaut.context.MessageSource;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import net.mzimmer.cloud.landingpage.adapter.active.ui.component.LanguageSwitcherComponent;
import net.mzimmer.cloud.landingpage.adapter.active.ui.component.MilestonesComponent;
import net.mzimmer.cloud.landingpage.adapter.active.ui.component.PageComponent;
import net.mzimmer.cloud.landingpage.adapter.active.ui.component.ParagraphsComponent;
import net.mzimmer.cloud.landingpage.domain.OverallProgress;
import net.mzimmer.html.Component2;
import net.mzimmer.html.Node;
import net.mzimmer.io.micronaut.i18n.I18n;

import javax.inject.Singleton;

import static net.mzimmer.html.Builder.t;

@Singleton
@AllArgsConstructor
public class LandingPageTemplate implements Component2<Option<OverallProgress>, MessageSource.MessageContext> {
    private final I18n i18n;
    private final PageComponent pageComponent;
    private final LanguageSwitcherComponent languageSwitcherComponent;
    private final ParagraphsComponent paragraphsComponent;
    private final MilestonesComponent milestonesComponent;

    @Override
    public Stream<Node> build(Option<OverallProgress> maybeOverallProgress, MessageSource.MessageContext messageContext) {
        return pageComponent.build(new PageComponent.Title(i18n.m(messageContext, "landingpage.section.introduction.title")),
                                   new PageComponent.Content(
                                       Stream.<Node>of(t("h1").a("class", "prose").c(i18n.m(messageContext, "landingpage.section.introduction.title")))
                                           .appendAll(paragraphsComponent.build(i18n.m(messageContext, "landingpage.section.introduction.text")))
                                           .appendAll(languageSwitcherComponent.build(messageContext))
                                           .append(t("h2").a("class", "prose").c(i18n.m(messageContext, "landingpage.section.progress.title")))
                                           .appendAll(milestonesComponent.build(maybeOverallProgress, messageContext))
                                           .append(t("h2").a("class", "prose").c(i18n.m(messageContext, "landingpage.section.stability.title")))
                                           .appendAll(paragraphsComponent.build(i18n.m(messageContext, "landingpage.section.stability.text")))
                                           .append(t("h3").a("class", "prose").c(i18n.m(messageContext, "landingpage.section.stability.phases.phase.alpha.title")))
                                           .appendAll(paragraphsComponent.build(i18n.m(messageContext, "landingpage.section.stability.phases.phase.alpha.text")))
                                           .append(t("h3").a("class", "prose").c(i18n.m(messageContext, "landingpage.section.stability.phases.phase.beta.title")))
                                           .appendAll(paragraphsComponent.build(i18n.m(messageContext, "landingpage.section.stability.phases.phase.beta.text")))
                                           .append(t("h3").a("class", "prose").c(i18n.m(messageContext, "landingpage.section.stability.phases.phase.release.title")))
                                           .appendAll(paragraphsComponent.build(i18n.m(messageContext, "landingpage.section.stability.phases.phase.release.text")))
                                   ),
                                   messageContext);
    }
}
