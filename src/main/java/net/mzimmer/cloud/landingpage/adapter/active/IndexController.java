package net.mzimmer.cloud.landingpage.adapter.active;

import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpParameters;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.Status;
import lombok.AllArgsConstructor;
import net.mzimmer.cloud.landingpage.adapter.active.ui.LandingPageTemplate;
import net.mzimmer.cloud.landingpage.application.ProgressApplicationService;
import net.mzimmer.html.Renderer;
import net.mzimmer.io.micronaut.i18n.MessageContextProvider;

import static io.micronaut.http.HttpStatus.OK;
import static io.micronaut.http.MediaType.TEXT_HTML;

@Controller
@AllArgsConstructor
public class IndexController {
    private final MessageContextProvider messageContextProvider;
    private final LandingPageTemplate landingPageTemplate;
    private final Renderer<String> renderer;
    private final ProgressApplicationService progressApplicationService;

    @Get
    @Status(OK)
    @Produces(TEXT_HTML)
    public String index(HttpParameters httpParameters, HttpHeaders httpHeaders) {
        return renderer.render(landingPageTemplate.build(progressApplicationService.getOverallProgress()
                                                                                   .extract()
                                                                                   .getMaybeOverallProgress(),
                                                         messageContextProvider.getMessageContext(httpParameters, httpHeaders)));
    }
}
