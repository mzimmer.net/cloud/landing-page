package net.mzimmer.cloud.landingpage.adapter.active.ui.component;

import io.micronaut.context.MessageSource;
import io.vavr.collection.Stream;
import lombok.AllArgsConstructor;
import net.mzimmer.cloud.landingpage.boot.I18nConfiguration;
import net.mzimmer.html.Component1;
import net.mzimmer.html.Node;
import net.mzimmer.io.micronaut.i18n.I18n;

import javax.inject.Singleton;

import static net.mzimmer.html.Builder.t;

@Singleton
@AllArgsConstructor
public class LanguageSwitcherComponent implements Component1<MessageSource.MessageContext> {
    private final I18n i18n;
    private final I18nConfiguration i18nConfiguration;

    @Override
    public Stream<Node> build(MessageSource.MessageContext messageContext) {
        final String currentLanguage = messageContext.getLocale().getLanguage();
        return i18nConfiguration.getAllowedLanguages()
                                .toStream()
                                .remove(currentLanguage)
                                .map(otherLanguage -> {
                                    final String text = String.format("components.languageSwitcher.from.%s.to.%s.text",
                                                                      currentLanguage,
                                                                      otherLanguage);
                                    return t("p").a("class", "prose").c(
                                        t("a").a("href", "?lang=" + otherLanguage).c(
                                            i18n.m(messageContext, text)
                                        )
                                    );
                                });
    }
}
