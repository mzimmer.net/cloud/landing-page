package net.mzimmer.cloud.landingpage.adapter.active.ui.component;

import io.micronaut.context.MessageSource;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import net.mzimmer.cloud.landingpage.domain.OverallProgress;
import net.mzimmer.html.Component2;
import net.mzimmer.html.Node;
import net.mzimmer.io.micronaut.i18n.I18n;

import javax.inject.Singleton;
import java.util.Locale;

import static net.mzimmer.html.Builder.t;

@Singleton
@AllArgsConstructor
public class MilestonesComponent implements Component2<Option<OverallProgress>, MessageSource.MessageContext> {
    private final I18n i18n;
    private final MilestoneComponent milestoneComponent;

    @Override
    public Stream<Node> build(Option<OverallProgress> maybeOverallProgress, MessageSource.MessageContext messageContext) {
        return Stream.of(
            t("div").a("class", "milestones").c(
                maybeOverallProgress.fold(() -> noData(messageContext), overallProgress -> data(overallProgress, messageContext))
            )
        );
    }

    private Stream<Node> data(OverallProgress overallProgress, MessageSource.MessageContext messageContext) {
        return milestoneComponent.build(new MilestoneComponent.Data(
            new MilestoneComponent.Title(i18n.m(messageContext, "components.milestones.overallprogress")),
            overallProgress.getOverallProgress(),
            overallProgress.getPhases().toStream().map(phaseId_phase -> {
                var phaseId = phaseId_phase._1();
                final String phaseTitle = i18n.m(messageContext, "landingpage.section.stability.phases.phase." + phaseId.name().toLowerCase(Locale.ROOT) + ".title");
                var phase = phaseId_phase._2();
                return new MilestoneComponent.Data(
                    new MilestoneComponent.Title(phaseTitle),
                    phase.getProgress(),
                    phase.getMilestones().toStream().map(milestone -> {
                        final String projectTitle = i18n.m(messageContext, "landingpage.section.stability.projects.project." + milestone.getProjectId().name().toLowerCase(Locale.ROOT) + ".title");
                        return new MilestoneComponent.Data(
                            new MilestoneComponent.Title(projectTitle),
                            milestone.getProgress(),
                            Stream.empty()
                        );
                    }).toStream()
                );
            }).toStream()
        ), messageContext);
    }

    private Stream<Node> noData(MessageSource.MessageContext messageContext) {
        return Stream.of(
            t("div").a("class", "milestone__box").c(
                t("div").a("class", "milestone__box__description").c(
                    t("p").c(i18n.m(messageContext, "components.milestones.nodata"))
                )
            )
        );
    }
}
