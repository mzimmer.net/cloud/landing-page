package net.mzimmer.cloud.landingpage.adapter.active;

import io.micronaut.scheduling.annotation.Scheduled;
import lombok.AllArgsConstructor;
import net.mzimmer.cloud.landingpage.application.ProgressApplicationService;

import javax.inject.Singleton;

@Singleton
@AllArgsConstructor
public class UpdateCacheScheduler {
    private final ProgressApplicationService progressApplicationService;

    @Scheduled(fixedRate = "${landingpage.gitlab.refreshInterval:5m}")
    void updateCache() {
        progressApplicationService.refreshMilestones().extract();
    }
}
