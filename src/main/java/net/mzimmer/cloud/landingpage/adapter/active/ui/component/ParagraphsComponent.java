package net.mzimmer.cloud.landingpage.adapter.active.ui.component;

import io.vavr.collection.Stream;
import net.mzimmer.html.Component1;
import net.mzimmer.html.Node;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Singleton;

import static net.mzimmer.html.Builder.t;

@Singleton
public class ParagraphsComponent implements Component1<String> {
    @Override
    public Stream<Node> build(String paragraphs) {
        return Stream.of(StringUtils.split(paragraphs, "\n\n"))
                     .map(paragraph -> t("p").a("class", "prose").c(paragraph));
    }
}
