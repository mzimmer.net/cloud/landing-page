package net.mzimmer.cloud.landingpage.adapter.active.ui.component;

import io.micronaut.context.MessageSource;
import io.vavr.collection.Stream;
import lombok.Value;
import net.mzimmer.html.Component3;
import net.mzimmer.html.Node;

import javax.inject.Singleton;

import static net.mzimmer.html.Builder.doctype;
import static net.mzimmer.html.Builder.t;

@Singleton
public class PageComponent implements Component3<PageComponent.Title, PageComponent.Content, MessageSource.MessageContext> {
    @Override
    public Stream<Node> build(Title title, Content content, MessageSource.MessageContext messageContext) {
        return Stream.of(
            doctype("html"),
            t("html").a("lang", messageContext.getLocale().getLanguage()).c(
                t("head").c(
                    t("meta").a("charset", "UTF-8"),
                    t("meta").a("name", "viewport").a("content", "width=device-width, initial-scale=1.0"),
                    t("meta").a("http-equiv", "X-UA-Compatible").a("content", "ie=edge"),
                    t("title").c(title.getValue()),
                    t("link").a("href", "assets/modern-normalize.1.0.0.min.css").a("rel", "stylesheet"),
                    t("link").a("href", "assets/landingpage.css").a("rel", "stylesheet")
                ),
                t("body").c(
                    t("main").c(content.getValue())
                )
            )
        );
    }

    @Value
    public static class Title {
        String value;
    }

    @Value
    public static class Content {
        Stream<Node> value;
    }
}
