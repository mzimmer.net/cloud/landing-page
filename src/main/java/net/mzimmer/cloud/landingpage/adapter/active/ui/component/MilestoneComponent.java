package net.mzimmer.cloud.landingpage.adapter.active.ui.component;

import io.micronaut.context.MessageSource;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import lombok.Value;
import net.mzimmer.cloud.landingpage.domain.Progress;
import net.mzimmer.html.Component2;
import net.mzimmer.html.Node;
import net.mzimmer.html.Tag;
import net.mzimmer.io.micronaut.i18n.I18n;

import javax.inject.Singleton;

import static net.mzimmer.html.Builder.t;

@Singleton
@AllArgsConstructor
public class MilestoneComponent implements Component2<MilestoneComponent.Data, MessageSource.MessageContext> {
    private final I18n i18n;

    @Override
    public Stream<Node> build(Data data, MessageSource.MessageContext messageContext) {
        final Tag box = t("div").a("class", "milestone__box").c(
            t("p").a("class", "milestone__box__title").c(data.getTitle().getValue()),
            t("div").a("class", "milestone__box__progress-bar-background").c(
                t("div").a("class", "milestone__box__progress-bar-background__progress-bar-foreground").a("style", "width: " + data.getProgress().getPercentage() + "%;")
            ),
            t("div").a("class", "milestone__box__description").c(
                t("p").c(
                    i18n.m(messageContext,
                           "components.milestone.counts",
                           data.getProgress().getNumberOfClosedIssues().getValue(),
                           data.getProgress().getNumberOfIssues().getValue())
                ),
                t("p").c(
                    i18n.m(messageContext,
                           "components.milestone.percentage",
                           data.getProgress().getPercentage())
                )
            )
        );
        final Option<Tag> maybeChildren = data.getChildren().headOption().map(
            firstChild -> t("div").a("class", "milestone__children").c(
                data.getChildren().flatMap(child -> build(child, messageContext))
            )
        );
        final Stream<Node> content = Stream.<Node>of(box).appendAll(maybeChildren);
        return Stream.of(
            t("div").a("class", "milestone").c(
                content
            )
        );
    }

    @Value
    public static class Data {
        Title title;
        Progress progress;
        Stream<Data> children;
    }

    @Value
    public static class Title {
        String value;
    }
}
