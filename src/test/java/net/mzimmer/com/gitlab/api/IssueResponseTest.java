package net.mzimmer.com.gitlab.api;

import com.fasterxml.jackson.core.type.TypeReference;
import net.mzimmer.cloud.landingpage.test.SerializationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class IssueResponseTest extends SerializationTest {
    @Test
    void shouldDeserialize() {
        // given
        String json = readResourceFile("net/mzimmer/com/gitlab/api/issueResponse.json");

        // when
        List<IssueResponse> result = deserialize(json, new TypeReference<>() {
        });

        // then
        assertThat(result).hasSize(10);
        assertThat(result).extracting(IssueResponse::getState).containsExactly("opened", "opened", "closed", "closed", "closed", "closed", "closed", "closed", "closed", "closed");
    }
}
