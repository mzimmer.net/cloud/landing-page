package net.mzimmer.com.gitlab.api;

import com.fasterxml.jackson.core.type.TypeReference;
import net.mzimmer.cloud.landingpage.test.SerializationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class MilestoneResponseTest extends SerializationTest {
    @Test
    void shouldDeserialize() {
        // given
        String json = readResourceFile("net/mzimmer/com/gitlab/api/milestoneResponse.json");

        // when
        List<MilestoneResponse> result = deserialize(json, new TypeReference<>() {
        });

        // then
        assertThat(result).hasSize(2);
        assertThat(result).extracting(MilestoneResponse::getId).containsExactly(1301197L, 1285422L);
        assertThat(result).extracting(MilestoneResponse::getTitle).containsExactly("Base", "Production ready");
    }
}
