package net.mzimmer.html;

import io.vavr.collection.Stream;
import org.junit.jupiter.api.Test;

import java.io.StringWriter;

import static net.mzimmer.html.Builder.doctype;
import static net.mzimmer.html.Builder.t;
import static org.assertj.core.api.Assertions.assertThat;

class HtmlTest {
    private final Stream<Node> document = Stream.of(
        doctype("html"),
        t("html").a("lang", "en").c(
            t("head").c(
                t("meta").a("charset", "UTF-8"),
                t("title").c("Title")
            ),
            t("body").c(
                t("main").c(
                    t("h1").c("Title"),
                    t("section").a("id", "sample-section").a("class", "example-class").c(
                        t("h1").c("Sample Section"),
                        t("p").c("Lorem ipsum"),
                        t("div").a("class", "hint desktop-only")
                    )
                )
            )
        )
    );
    private final String output = "<!doctype html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>Title</title></head><body><main><h1>Title</h1><section id=\"sample-section\" class=\"example-class\"><h1>Sample Section</h1><p>Lorem ipsum</p><div class=\"hint desktop-only\"></div></section></main></body></html>";

    @Test
    void stringRenderer() {
        // given
        Renderer<String> renderer = new StringRenderer();

        // when
        String result = renderer.render(document);

        // then
        assertThat(result).isEqualTo(output);
    }

    @Test
    void writerRenderer() {
        // given
        StringWriter stringWriter = new StringWriter();
        Renderer<Void> renderer = new WriterRenderer(stringWriter);

        // when
        renderer.render(document);

        // then
        assertThat(stringWriter.toString()).isEqualTo(output);
    }
}
