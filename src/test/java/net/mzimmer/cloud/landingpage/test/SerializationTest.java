package net.mzimmer.cloud.landingpage.test;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.context.ApplicationContext;
import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class SerializationTest {
    @SneakyThrows
    protected String readResourceFile(String path) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(path);
        assertThat(inputStream).isNotNull();
        return new BufferedReader(
            new InputStreamReader(inputStream, StandardCharsets.UTF_8))
            .lines()
            .collect(Collectors.joining("\n"));
    }

    @SneakyThrows
    protected <T> T deserialize(String json, TypeReference<T> type) {
        ApplicationContext applicationContext = ApplicationContext.run("test");
        ObjectMapper objectMapper = applicationContext.getBean(ObjectMapper.class);
        return objectMapper.readValue(json, type);
    }
}
