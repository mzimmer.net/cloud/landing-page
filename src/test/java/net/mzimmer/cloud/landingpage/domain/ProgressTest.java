package net.mzimmer.cloud.landingpage.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class ProgressTest {
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 7, 10, 99, 100, 101})
    void shouldBeComplete(int numberOfIssues) {
        // given
        Progress progress = createProgress(numberOfIssues, numberOfIssues);

        // when
        int result = progress.getPercentage();

        // then
        assertThat(result).isEqualTo(100);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 7, 10, 99, 100, 101})
    void shouldBeIncomplete(int numberOfIssues) {
        // given
        Progress progress = createProgress(0, numberOfIssues);

        // when
        int result = progress.getPercentage();

        // then
        assertThat(result).isEqualTo(0);
    }

    @Test
    void shouldBeHalf() {
        // given
        Progress progress = createProgress(3, 6);

        // when
        int result = progress.getPercentage();

        // then
        assertThat(result).isEqualTo(50);
    }

    @Test
    void shouldBeAlmostComplete() {
        // given
        Progress progress = createProgress(299, 300);

        // when
        int result = progress.getPercentage();

        // then
        assertThat(result).isEqualTo(99);
    }

    private Progress createProgress(int numberOfClosedIssues, int numberOfIssues) {
        return new Progress(new NumberOfClosedIssues(numberOfClosedIssues), new NumberOfIssues(numberOfIssues));
    }
}
